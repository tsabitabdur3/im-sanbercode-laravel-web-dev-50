<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $sheep = new animal("shaun");

    echo " Nama : " . $sheep->name . "<br>"; 
    echo " Legs : " . $sheep->legs . "<br>"; 
    echo " Cold Blooded : " . $sheep->cold_blooded . "<br>" . "<br>";

    $frog = new Frog ("buduk");

    echo " Nama : " . $frog->name . "<br>";
    echo " Legs : " . $frog->legs . "<br>";
    echo " Cold Blooded : " . $frog->cold_blooded . "<br>";
    echo " Hop Hop " . $frog->jump() . "<br>" . "<br>";

    $ape = new Ape ("Kera Sakti");

    echo " Nama : " . $ape->name . "<br>";
    echo " Legs : " . $ape->legs . "<br>";
    echo " Cold Blooded : " . $ape->cold_blooded . "<br>";
    echo " Auooo " . $ape->yell();

?>