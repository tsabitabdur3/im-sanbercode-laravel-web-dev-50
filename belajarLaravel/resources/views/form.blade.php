@extends('layouts.master')

@section('title')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <form action="/kirim" method="post">
        @csrf
        <label>First Name:</label> <br>
        <input type="text" name="first"> <br> <br>
        <label>Last Name:</label> <br>
        <input type="text" name="last"> <br> <br>
        <label>Gender:</label> <br>
        <input type="radio" name="Gender"> Male <br>
        <input type="radio" name="Gender"> Female <br>
        <input type="radio" name="Gender"> Other <br> <br>
        <label>Nationality:</label> <br>
        <select name="Nationality">
            <option value="">Indonesian</option>
            <option value="">Malaysian</option>
            <option value="">Japanese</option>
            <option value="">Italian</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br> <br>
        <label>Bio:</label> <br>
        <textarea cols="30" rows="10"></textarea> <br> <br>

        <input type="submit" value="Sign Up">
    </form>
@endsection