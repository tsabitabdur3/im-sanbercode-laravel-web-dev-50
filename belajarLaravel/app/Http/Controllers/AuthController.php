<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formulir()
    {
        return view('form');
    }

    public function kirim(Request $request)
    {
        $awal = $request['first'];
        $akhir = $request['last'];

        return view('welcome', ['awal' => $awal, 'akhir' => $akhir ]);
    }
}
